package demo;

import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import demo.api.executor.ResponseView;

/**
 * @author Allen
 */
public interface ExecutorWebService {
    @GET
    @Path("/executor")
    ResponseView create();
}
