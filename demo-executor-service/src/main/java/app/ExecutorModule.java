package app;

import app.demo.executor.service.ExecutorService;
import app.demo.executor.web.ExecutorWebServiceImpl;
import core.framework.module.Module;
import demo.ExecutorWebService;

/**
 * @author Allen
 */
public class ExecutorModule extends Module {
    @Override
    protected void initialize() {
        executor().add();
        bind(ExecutorService.class);
        api().service(ExecutorWebService.class, bind(ExecutorWebServiceImpl.class));
    }
}
