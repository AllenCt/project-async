package app.demo.executor.service;

import core.framework.async.Executor;
import core.framework.inject.Inject;
import demo.api.executor.ResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Allen
 */
public class ExecutorService {
    private final Logger logger = LoggerFactory.getLogger(ExecutorService.class);
    @Inject
    Executor executor;

    public ResponseView createExecutor() {
        executor.submit("create-executor", () -> {
            System.out.println("start");
            System.out.println(Thread.currentThread().getName());
            Thread.sleep(3000);
            System.out.println("end");
        });
        ResponseView responseView = new ResponseView();
        responseView.flag = true;
        return responseView;
    }
}
