package app.demo.executor.web;

import app.demo.executor.service.ExecutorService;
import core.framework.inject.Inject;
import demo.ExecutorWebService;
import demo.api.executor.ResponseView;

/**
 * @author Allen
 */
public class ExecutorWebServiceImpl implements ExecutorWebService {
    @Inject
    ExecutorService executorService;

    @Override
    public ResponseView create() {
        return executorService.createExecutor();
    }
}
