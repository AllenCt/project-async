import app.DemoExecutorApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoExecutorApp().start();
    }
}
