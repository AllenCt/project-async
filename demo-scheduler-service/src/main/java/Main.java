import app.DemoSchedulerApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoSchedulerApp().start();
    }
}
