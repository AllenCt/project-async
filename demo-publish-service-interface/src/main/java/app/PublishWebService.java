package app;

import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface PublishWebService {
    @GET
    @Path("/publish")
    void publishTest();

}
