package app.demo.api;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class TestPublishMessage {
    @Property(name = "publish_id")
    public Integer publishId;

    @Property(name = "publish_name")
    public String publishName;

}
