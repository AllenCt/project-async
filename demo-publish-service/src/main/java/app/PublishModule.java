package app;

import app.demo.api.TestPublishMessage;
import app.demo.publish.service.PublishService;
import app.demo.publish.web.PublishWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class PublishModule extends Module {
    @Override
    protected void initialize() {
        kafka().uri(requiredProperty("app.kafka.uri"));
        kafka().publish("test-topic", TestPublishMessage.class);
        bind(PublishService.class);
        api().service(PublishWebService.class, bind(PublishWebServiceImpl.class));
    }
}
