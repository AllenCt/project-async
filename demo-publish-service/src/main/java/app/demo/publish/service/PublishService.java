package app.demo.publish.service;

import app.demo.api.TestPublishMessage;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;

/**
 * @author Allen
 */
public class PublishService {
    @Inject
    private MessagePublisher<TestPublishMessage> publisher;

    public void publish() {
        TestPublishMessage publishMessage = new TestPublishMessage();
        publishMessage.publishName = "allen";
        publishMessage.publishId = 1;
        publisher.publish("publish", publishMessage);
    }
}
