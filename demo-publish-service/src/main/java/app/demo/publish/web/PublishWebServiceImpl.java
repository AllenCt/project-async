package app.demo.publish.web;

import app.PublishWebService;
import app.demo.publish.service.PublishService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class PublishWebServiceImpl implements PublishWebService {
    @Inject
    PublishService publishService;

    @Override
    public void publishTest() {
        publishService.publish();
    }
}
