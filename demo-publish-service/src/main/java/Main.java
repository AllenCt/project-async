import app.DemoPublishApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoPublishApp().start();
    }
}
