package app.demo.subcribe;

import app.demo.api.TestPublishMessage;
import core.framework.kafka.MessageHandler;


/**
 * @author Allen
 */
public class TestMessageHandler implements MessageHandler<TestPublishMessage> {
    @Override
    public void handle(String key, TestPublishMessage value) throws Exception {
        System.out.println("the key is " + key);
        System.out.println("the value id is " + value.publishId);
        System.out.println("the value name is " + value.publishName);
    }
}
