package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class DemoSubcribeApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("app.properties"));
        http().httpPort(8084);
        load(new SubcribeModule());
    }
}
